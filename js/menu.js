let menuLinks = document.querySelectorAll('header > nav a');
let menuToggler = document.getElementById('menu-toggler');

menuLinks.forEach(link => {
    link.addEventListener('click', function(event) {
        menuToggler.checked = false;
    })
});